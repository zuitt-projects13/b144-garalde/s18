console.log('S18-Activity')

// 3. Create a trainer object using object literals. 
// 4. Initialize/add the following trainer object properties: 
let trainer = {
	name : "Ash Ketchum",
	age : 10,
	pokemon : ['Pikachu','Charizard','Squirtle','Bulbasaur'],
	friends : {
		hoenn : ['May','Max'],
		kanto : ['Brock','Misty'],
	},
	talk: function() {
		console.log('Pikachu! I choose you! ')
	}
}

console.log(trainer)

console.log('Result of dot notation: ')
console.log(trainer.name)
console.log('Result of square bracket notation: ')
console.log(trainer['pokemon'])

console.log('Result of talk method: ')
trainer.talk()

function Pokemon(name,level){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level

	
	this.tackle = function(target){
		console.log( this.name + ' tackled ' + target.name)
		target.health -= this.attack
		console.log(`${target.name}'s health is now reduced to ${target.health}`)
		if (target.health <= 0){
			this.faint(target)
		}
	}

	this.faint = function(target) {
		console.log(`${target.name} has fainted.`)
		console.log(target)
	}

	
}

let pikachu = new Pokemon('Pikachu',12)
let geodude = new Pokemon('Geodude',8)
let mewtwo = new Pokemon('Mewtwo',100)

console.log('Initial stats:')
console.log(pikachu)
console.log(geodude)
console.log(mewtwo)


console.log('Fight!')
geodude.tackle(pikachu)
mewtwo.tackle(geodude)




