console.log('Session 18 JavaScript - Object')

let grades = [98.5,94.3,89.2,90.1]

console.log(grades[0])

// An object is similar to an arraw. It is a collection of related date and /or funtonalities usually it represent real world object.

let grade = {
	// object initializer/ literal notation
	// the object consists of properties which are used to describe an object. key-value pair
	// each data have specific label 
	math: 89.2,
	english: 98.5,
	science: 90.1,
	Filipino:94.3
}


// we use dot notation to access the properties/value of our object
/*syntax:
	let objectName = {
		keyA = valueA,
		keyB = valueB,
	}

	objectName.key


*/
console.log(grade.english)

let cellphone = {
	brandName: 'Nokia 3310',
	color: 'Dark blue',
	manufactureDate: 1999
}

	console.log(typeof cellphone)  

// dont forget the ','
let student = {
	firstName: 'John',
	lastName: 'Smith',
	location: {
		city: 'Tokyo',
		country: 'Japan'
	},

	emails: ['john@mail.com', 'johnsmith@mail.xyz'],

	// function inside an object
	// 'this' refer to the OWNER of the function
	fullName: function(){
		return this.firstName + ' ' + this.lastName
	}
}



console.log(student.location.city)


// bracket notation
console.log(student['firstName'])

console.log(student.emails)
console.log(student.emails[0])


console.log(student.fullName())

// sample for 'this'
const person1 = {
	name: 'Jane',
	greeting: function(){
		return 'Hi I\'m ' + this.name
	}
}

console.log(person1.greeting())

// end sample for 'this'



// ARRAY OF OBJECTS
// dont forget the ','
let contactList = [
	{
		firstName: 'John',
		lastName: 'Smith',
		location: 'Japan'
	},
	{
		firstName: 'Jane',
		lastName: 'Smith',
		location: 'Japan'
	},
	{
		firstName: 'Jasmine',
		lastName: 'Smith',
		location: 'Japan'
	}

]

console.log(contactList[0].firstName)


let people = [
	{
		name: 'Juanita',
		age: 13
	},
	{
		name: 'Juanito',
		age: 13
	}
]


people.forEach(function(person){console.log(person.name)})


// using template literal
console.log(`${people[0].name} are the list`)


// CONSTRUCTOR FUNCTION
// create object using contructor functions (also known as JS OBJECT CONSTRUCTOR/OBJECT FROM BLUEPRINTS) 
// creates reusable function to create several object that have the same data structure
// This is useful for creating multiple copies/instances of an object
// object literals
// let object = {}
// let object = new object
// instance - distinct/unique object. is a concrete occurence of any object which emphasizes on the unque identity of it
/*Syntax:
	function ObjectName(keyA,keyB) {
	 this.keyA = keyA
	 this.keyB = keyB
	}

*/

function Laptop(name, manufactureDate){
	// 'this' allows to assign a new object property by associating them with values received from our parameter
	this.name = name,
	this.manufactureDate = manufactureDate
}

// this is a unique instance of the laptop object
// 'new' - creates new instance /object
let laptop = new Laptop('lenovo',2008)

console.log('Result from creating obejcts from using object constructors')
console.log(laptop)

// this is ANOTHER unique instance of the laptop object

let myLaptop = new Laptop('Macbook Air',2020)

console.log('Result from creating obejcts from using object constructors')
console.log(myLaptop)

let oldLaptop = Laptop('Portal R2E CCMC',1980)

console.log(oldLaptop)

// creating empty objects
let computer = {}
let myComputer = new Object();

console.log(myLaptop.name)
console.log(myLaptop['name'])


let array = [laptop, myLaptop]

console.log(array[0].name)

// this also works but confusing
console.log(array[0]['name'])

// initializing/adding/deleting/reassigning object properties

// initialized/ added properties after the object was created. this is useful when an objects's properties are undetermined at the time of creating them

let car = {}

// add an object properties, we can use dot notation
car.name = 'Honda Civic'
console.log('initial:')
console.log(car)

car['manufactureDate'] = 2019

console.log(car)

// reassigning object properties
car.name = 'Dodge Charger R/T'
console.log(car)

// deleting object properties
delete car['manufactureDate']
console.log('result from deleting properties')
console.log(car)

// OBJECT METHODS
// a METHOD is a FUNCTION which is a property of an object

// they are also functions and one of the key difference that have is that methods are functions related to specific object

let person = {
	name: 'John',
	talk: function () {
		console.log('Hello my name is ' + this.name)
	}
}

console.log(person)
console.log('result from the object methods')
person.talk()
person.walk = function () {
	console.log(this.name + ' walked 25 steps forward')
}

person.walk()


let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city:'austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com','joesmith@mail.xyz'],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName)
	}
}

friend.introduce()

/*
Real World Application of Objects

-Scenario
1. we would like to create a game that would have several pokemon interact with each other
2. every pokemon would have the same set of stats, properties and functions


*/

// using object literals
let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This pokemon tackled target pokemon')
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	},
	faint: function(){
		console.log('Pokemon fainted.')
	}
}

// creating an object constructor instead of object literals
function Pokemon(name, level) {
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level,

	// methods

	this.tackle = function(target) {
		console.log( this.name + ' tackled ' + target.name)
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	},

	this.faint = function(){
		console.log(this.name + 'fainted.')
	}
}

let pikachu = new Pokemon('Pikachu',16)

let charizard = new Pokemon('Charizard',8)

pikachu.tackle(charizard)